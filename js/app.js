"use strict";

const Swipe = () => {
 this.element = 
 this.locked = null;
 this.isAcept = null; 
};


Swipe.prototype.rotate  = (deg) => {
 this.element.style.webkitTransform = "rotate("+deg+"deg)";
 this.element.style.mozTransform = "rotate("+deg+"deg)";
 this.element.style.msTransform = "rotate("+deg+"deg)";
 this.element.style.oTransform = "rotate("+deg+"deg)";
 this.element.style.transform = "rotate("+deg+"deg)";
};

Swipe.prototype.move    = (px, cores = {yes: "green", no: "red", nil: "white"}) => {
 this.element.style.marginLeft = (px%70)+"px";
 this.element.style.backgroundColor = px == 0 ? cores.nil : (px > 0 ? cores.yes : cores.no );
};

Swipe.prototype.touches = () => {};
Swipe.prototype.click   = () => {};
Swipe.prototype.drag    = () => {};
Swipe.prototype.drop    = () => {};